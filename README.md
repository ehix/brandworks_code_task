# Coding task

## Contents:
1. [Overview](#overview)
2. [Requirements](#requirements)
3. [Usage](#usage)
4. [Application](#application)
5. [API](#api)
    1. [Upload Image](#upload-image)
    2. [Get Metadata](#get-metadata)
    3. [Submit Query](#submit-query)
    4. [Purge Database](#purge-database)
6. [Specification](#specification)
---

## Overview:

The following contains documentation and discussion surrounding both an API and static web application that perform the same function. Either route allows the operator to upload an image, retrieve image metadata from a database, and also perform simple key-value queries on the database.

Uploaded images are stored under `./images` at the top level directory. Similarly, the database is found at the same level, under the name `db.json`. Both of these are constructed once the application is run for the first time.

The application will only allow images to be uploaded. This is largely due to me misinterpreting the brief, however feel as though the current application could be easily extended to handle `XMP` data as well.

A NoSQL database was chosen for its ability to store and model incoming data in a variety of forms, ranging from structured to unstructured, as they are not constrained by a schema. This is imperative as many of the uploaded images will be without some elements of metadata, and so are inconsistent between images. Similarity, in this context, a NoSQL database would provide a small performance benefit, by removing the overhead needed to translate database entries into objects when used in application. Finally, as NoSQL databases are horizontally scalable, an increase in load requires a proportional increase in the availability of servers.

With respect to search, as a NoSQL database scales, more complex search strategies and methods would need to be introduced to return information in a timely manner. Currently, only a key-value search is offered, and so would be quite inefficient and cumbersome to search for entries containing a specific timestamp, such as `date_time_original`. In this example, sorting the database entries by range index would be suitable, as this data type allows for a logically distinct way of sorting items in ascending/descending order, then a search query for a partial date could easily be found.

---
## Requirements
Use [pip](https://pip.pypa.io/en/stable/) or [conda](https://docs.conda.io/en/latest/) to install the requirements to run.

```bash
pip install -r requirements.txt

conda install --file requirements.txt
```

---
## Usage

From the top level directory, start the application using:

```bash
python run.py
```

Navigate to http://localhost:8080/

All responses are of the form:

```json
{
    "status": "HTTP status for the request",
    "data": "Mixed type holding the content of the response"
}
```
---
## Application:

A basic application is offered to help expediate the upload of image files, collect the metadata on images stored in the database, and to query the database for images that correspond with given key-value pairs.

The simple user interface allows operators to navigate between views, and interact with the application via forms. Once submitted, each form will return the operator back to the home page, with the returned request shown at the top of the screen. If a request is unsuccessful or invalid, an error message will be shown on the current page to inform the operator.

---
## API:

### Upload Image:
**Definition**

POST /upload

Upload image to database.

**Example**

```bash
curl -v -X POST -H "Content-Type: multipart/form-data" -F "file=@C:\Users\user\Pictures\test_image.jpg" http://localhost:8080/upload
```

**Response**

```json
{
  "data": {
    "file": "test_image.jpg"
  },
  "status": 201
}
```

### Get Metadata
**Definition**

GET /meta/<string:uuid>

The database entry with the corresponding UUID is returned.

**Example**

`localhost:8080/meta/f426b98cc7754455bda0e5930fb2f8a9`

**Response**

```json
{
  "data": {
      "meta": {
          "0": {
              "uuid": "f426b98cc7754455bda0e5930fb2f8a9",
              "filepath": "./images",
              "filename": "test_image.jpg",
              "extension": "jpg",
              "mime": "jpeg",
              "date_time_original": "2009:06:08 15:34:48",
              "create_date": "2009:06:08 15:34:48",
              "modify_date": "2009:06:08 15:34:48"
              }
          }
      }
  },
  "status": 200
}
```

### Submit Query
**Definition**

GET /query/<string:uuid>&<string:value>

Each database entry which contains the corresponding key-value pair is returned.

**Example**

`localhost:8080/query/mime&jpeg`

**Response**

```json
{
  "data": {
      "query": {
          "0": {
              "uuid": "555699bd875c476eadd44451779de09b",
              "filepath": "./images",
              "filename": "test_image.jpg",
              "extension": "jpg",
              "mime": "jpeg",
              "date_time_original": null,
              "create_date": null,
              "modify_date": "2017:10:12 19:19:12"
          },
          "1": {
              "uuid": "6fdd175b700545cbbbe245569f1b5e58",
              "filepath": "./images",
              "filename": "test_image2.jpg",
              "extension": "jpg",
              "mime": "jpeg",
          }
      }
  },
  "status": 200
}
```

### Purge Database
**Definition**

GET /purge

The database is purged (used mainly for testing).

**Example**

`localhost:8080/purge`

**Response**
```json
{
  "data": {
    "purge": null
  },
  "status": 200
}
```

---
## Specification
Your primary task is to build an API to store and retrieve/query metadata extracted from files.

In this task, as in our business, performance and scalability are key factors.
It would be great if the architecture of the application reflected these constraints.


### Requirements
* Python must be used as a programming language.
* README file explaining how to run your service and describing how you would extend the API to store a
  large amount of data(millions/billions) and have it queryable way. Please try to add as much detail as you can.

### Endpoints

#### Extract metadata
This endpoint will allow the user to upload a file to the system in order to extract its metadata(any file type/ any size).

Internally the endpoint is responsible to extract the metadata of the file and store the file's metadata
in a way to be possible to query it via another endpoint.

Note: _Metadata is a very unstructured piece of data, but there are certain data we want to store in a more structured way:_
* Extraction id - a generated unique id that is recorded on every extraction
* Date Created - the date that the file was created, should be obtained from the metadata, this will be located in several fields of the metadata
* Date Created - the date that the file was last modified, should be obtained from the metadata, this will be located in several fields of the metadata
* File Path - the path where the file was uploaded
* File Name - the original file name
* File Extension - the extension of the file
* File Mime Type - the mime-type of the file uploaded

Examples of where dates could be found(just few examples):
* EXIF: DateTimeOriginal
* XMP: DateCreated

* EXIF: ModifyDate
* XMP: ModifyDate

#### Get metadata
This endpoint will retrieve the metadata stored in the system for a given Extraction id

#### Query metadata
This endpoint will retrieve all the assets in the system that have a given metadata key:value

Example: GET endpoint?tag=<tag>&value=<value>


### Notes
* Feel free to choose the technologies/tools from the frameworks/libraries to the data storage.
* It may be easier to give us a link to a git repo when you're done, otherwise a compressed git archive would be fine.
