from tinydb import TinyDB, Query, where


class DB():
    def __init__(self):
        self.db = TinyDB('db.json')

    def purge(self):
        self.db.truncate()
        self.db.all()

    def insert(self, entry):
        self.db.insert(entry)

    def locate(self, uuid):
        return self.db.search(where('uuid') == uuid)

    def query(self, key, value):
        return [entry for entry in self.db.all() if entry.get(key, None) == value]
