import os
import json

from .api import db, UPLOAD_DIR
from .utils import extract_meta

import werkzeug
from werkzeug.utils import secure_filename
from flask_restful import Resource, reqparse
from flask import jsonify
from http import HTTPStatus


class Purge(Resource):
    def get(self):
        db.purge()
        return jsonify({"status": HTTPStatus.OK, "data": {"purge": None}})


class Upload(Resource):
    def post(self):
        parse = reqparse.RequestParser()
        parse.add_argument(
            "file", type=werkzeug.datastructures.FileStorage, location='files')
        file = parse.parse_args().get("file")
        filepath = os.path.join(UPLOAD_DIR, secure_filename(file.filename))
        file.save(filepath)
        db.insert(extract_meta(filepath))
        return jsonify({"status": HTTPStatus.CREATED, "data": {"file": file.filename}})


class Meta(Resource):
    def get(self, uuid):
        match = db.locate(uuid)
        if match:
            return jsonify({"status": HTTPStatus.OK, "data": {"meta": json.dumps(to_dict(match), sort_keys=False)}})


class Query(Resource):
    def get(self, key, value):
        match = db.query(key, value)
        if match:
            return jsonify({"status": HTTPStatus.OK, "data": {"query": json.dumps(to_dict(match), sort_keys=False)}})
