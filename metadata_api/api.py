import os
import json

from .db import DB
from .utils import extract_meta, to_dict

from flask import Flask, flash, render_template, request, jsonify, redirect, url_for
from flask_restful import Resource, Api
from werkzeug.utils import secure_filename

app = Flask(__name__)
api = Api(app)
db = DB()

UPLOAD_DIR = './images'

app.secret_key = b'secret'
app.config['UPLOAD_DIR'] = UPLOAD_DIR

from .resources import Purge, Upload, Meta, Query

api.add_resource(Purge, '/purge')
api.add_resource(Upload, '/upload')
api.add_resource(Meta, '/meta/<string:uuid>')
api.add_resource(Query, '/query/<string:key>&<string:value>')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/view_upload', methods=['GET', 'POST'])
def view_upload():
    error = None
    if request.method == 'POST':
        file = request.files['file']
        filename = secure_filename(file.filename)
        if filename != '':
            filepath = os.path.join(app.config['UPLOAD_DIR'], filename)
            file.save(filepath)
            db.insert(extract_meta(filepath))
            flash("File uploaded successful")
            return redirect(url_for('index'))
        else:
            error = 'No file selected'
    return render_template('upload.html', error=error)


@app.route('/view_get_meta', methods=['GET', 'POST'])
def view_get_meta():
    error = None
    if request.method == 'POST':
        uuid = request.form['uuid']
        match = db.locate(uuid)
        if match:
            flash(json.dumps(to_dict(match), sort_keys=False))
            return redirect(url_for('index'))
        else:
            error = "Could not find file requested"
    return render_template('get_meta.html', error=error)


@app.route('/view_query_meta', methods=['GET', 'POST'])
def view_query_meta():
    error = None
    if request.method == 'POST':
        key, value = request.form['key'].lower(), request.form['value'].lower()
        match = db.query(key, value)
        if match:
            flash(json.dumps(to_dict(match), sort_keys=False))
            return redirect(url_for('index'))
        else:
            error = "No results for given criteria"
    return render_template('query_meta.html', error=error)
