import os
import uuid
from collections import defaultdict

from PIL import Image, ExifTags


def to_dict(data):
    return {i: d for i, d in enumerate(data)}


def extract_meta(filepath):
    # https://exiftool.org/TagNames/EXIF.html

    img = Image.open(filepath)
    metadata = defaultdict()
    metadata['uuid'] = uuid.uuid4().hex
    metadata['filepath'], metadata['filename'] = os.path.split(filepath)
    _, metadata['extension'] = metadata.get('filename', None).split('.')
    metadata['mime'] = img.format.lower()

    try:
        exif_tags = {ExifTags.TAGS[k]: v for k,
                     v in img._getexif().items() if k in ExifTags.TAGS}
        metadata['date_time_original'] = exif_tags.get(
            'DateTimeOriginal', None)  # 36867
        metadata['create_date'] = exif_tags.get(
            'DateTimeDigitized', None)  # 36868
        metadata['modify_date'] = exif_tags.get('DateTime', None)  # 306
    except AttributeError:
        pass
    return metadata
